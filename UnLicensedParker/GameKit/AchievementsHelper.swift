//
//  AchievementsHelper.swift
//  UnLicensedParker
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import GameKit

class AchievementsHelper {
   static let sharedInstance = AchievementsHelper()
    
    static let monete25 = 25.0
    static let monete50 = 50.0
    static let monete75 = 75.0
    static let monete100 = 100.0
    
    static let MaxSoldi = 1.0
    static let maxImpact = 1.0
    static let maxImpactMoto = 1.0
    static let niceStart = 1.0
//    Bundle ID Achievement
    
    static let firstPlayID = "com.luigi.unlicensedparker.first"
    //Supera il primo parcheggiatore Abusivo 
    static let niceStartID = "com.luigi.unlicensedparker.niceStart"
//    Impatto con il primo parcheggiatore abusivo
    static let impactParkerID = "com.luigi.unlicensedparker.capo"
    
    static let impactMotoID = "com.luigi.unlicensedparker.motorino"
    
    static let risparmio25ID = "com.luigi.unlicensedparker.25"
    
    static let risparmio50ID = "com.luigi.unlicensedparker.50"
    
    static let risparmio75ID = "com.luigi.unlicensedparker.75"
    
    static let risparmio100ID = "com.luigi.unlicensedparker.100"
 
    func firstPlayAchievement(noOfSoldi: Int) -> GKAchievement
    {
      //1
      let percent = Double(noOfSoldi)/AchievementsHelper.MaxSoldi * 100

      //2
      let firstPlayAchievement = GKAchievement(identifier: AchievementsHelper.firstPlayID)

      //3
      firstPlayAchievement.percentComplete = percent
      firstPlayAchievement.showsCompletionBanner = true
      return firstPlayAchievement
    }
    
    func niceStartAchievement(noOfParker: Int) -> GKAchievement
    {
        let percent = Double(noOfParker)/AchievementsHelper.niceStart * 100
        
        let niceStartAchievement = GKAchievement(identifier: AchievementsHelper.niceStartID)
        niceStartAchievement.percentComplete = percent
        niceStartAchievement.showsCompletionBanner = true
        return niceStartAchievement
    }
    func impactParkerAchievement(noOfImpact: Int) -> GKAchievement {
        let percent = Double(noOfImpact)/AchievementsHelper.maxImpact * 100
        
        let impactParkerAchievement = GKAchievement(identifier: AchievementsHelper.impactParkerID)
        impactParkerAchievement.percentComplete = percent
        impactParkerAchievement.showsCompletionBanner = true
        return impactParkerAchievement
    }
    
    func impactMotoAchievement(noOfImpactMoto: Int) -> GKAchievement {
           let percent = Double(noOfImpactMoto)/AchievementsHelper.maxImpactMoto * 100
           
           let impactMotoAchievement = GKAchievement(identifier: AchievementsHelper.impactMotoID)
           impactMotoAchievement.percentComplete = percent
           impactMotoAchievement.showsCompletionBanner = true
           return impactMotoAchievement
       }
    
    func risparmio25Achievement(noOfMonete: Int) -> GKAchievement {
              let percent = Double(noOfMonete)/AchievementsHelper.monete25 * 100
              
              let risparmio25 = GKAchievement(identifier: AchievementsHelper.risparmio25ID)
              risparmio25.percentComplete = percent
              risparmio25.showsCompletionBanner = true
              return risparmio25
          }
    func risparmio50Achievement(noOfMonete: Int) -> GKAchievement {
                 let percent = Double(noOfMonete)/AchievementsHelper.monete50 * 100
                 
                 let risparmio50 = GKAchievement(identifier: AchievementsHelper.risparmio50ID)
                 risparmio50.percentComplete = percent
                 risparmio50.showsCompletionBanner = true
                 return risparmio50
             }
    func risparmio75Achievement(noOfMonete: Int) -> GKAchievement {
                 let percent = Double(noOfMonete)/AchievementsHelper.monete75 * 100
                 
                 let risparmio75 = GKAchievement(identifier: AchievementsHelper.risparmio75ID)
                 risparmio75.percentComplete = percent
                 risparmio75.showsCompletionBanner = true
                 return risparmio75
             }
    func risparmio100Achievement(noOfMonete: Int) -> GKAchievement {
                 let percent = Double(noOfMonete)/AchievementsHelper.monete100 * 100
                 
                 let risparmio100 = GKAchievement(identifier: AchievementsHelper.risparmio100ID)
                 risparmio100.percentComplete = percent
                 risparmio100.showsCompletionBanner = true
                 return risparmio100
             }
    
}


//
//  GameCenterHelper.swift
//  UnLicensedParker
//
//  Created by Silvio Fosso on 11/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit
import Foundation
import GameKit

class GameKitHelper: NSObject {
   
  static let sharedInstance = GameKitHelper()
    static let PresentAuthenticationViewController = "PresentAuthenticationViewController"
    
  var authenticationViewController: UIViewController?
  var gameCenterEnabled = false
    
    func authenticateLocalPlayer() {
      // 1
        GKLocalPlayer.local.authenticateHandler =
          { (viewController, error) in
        // 2
        self.gameCenterEnabled = false
        if viewController != nil {
          // 3
          self.authenticationViewController = viewController

          NotificationCenter.default.post(name: NSNotification.Name(
            GameKitHelper.PresentAuthenticationViewController),
            object: self)
        } else if GKLocalPlayer.local.isAuthenticated {
          // 4
          self.gameCenterEnabled = true
        }
      }
    }
    
    func reportScore(score: Int64, forLeaderboardID leaderboardID:
        String, errorHandler: ((Error?)->Void)? = nil) {
      guard gameCenterEnabled else {
        return
      }

      //1
      let gkScore = GKScore(leaderboardIdentifier: leaderboardID)
      gkScore.value = score

      //2
      GKScore.report([gkScore], withCompletionHandler: errorHandler)
        
    }

   func reportAchievements(achievements: [GKAchievement],
     errorHandler: ((Error?)->Void)? = nil) {

     guard gameCenterEnabled else {
       return
     }

     GKAchievement.report(achievements,
       withCompletionHandler: errorHandler)
   }
}


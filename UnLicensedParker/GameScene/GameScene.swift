//
//  GameScene.swift
//  UnLicensedParker
//
//  Created by Silvio Fosso on 24/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import SpriteKit
import GameplayKit
import AudioToolbox
import AVFoundation
import UIKit
import GameKit
class GameScene: SKScene,SKPhysicsContactDelegate, GKGameCenterControllerDelegate {
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    var impactMoto = 0
    var impact = 0
    var firstParker = 0
    var categoryp = UInt32(0b01) // 1
    var tempo : CGFloat = 3.0
    var categoryn = UInt32(0b10) // 2
    var categoryn2 = UInt32(0b11)
    var categoryg : UInt32 = 0x1 << 5
    var min = 2.0
    var max = 3.5
    var tapCount = 0
    var valore = 1
    var soldi = 0
    var label = SKLabelNode()
    var rando : CGFloat = 0
    var predi : Bool = false
    var muovichild = true
    var SuonoCoins = AVAudioPlayer()
    var SuonoTap = AVAudioPlayer()
    var sound = false
    var doppioSalto = false
    public var portaScore = [Int]()
    var Spawn = 2.0
    
    // Parcheggiatore
    let parker = SKSpriteNode(imageNamed: "sprite_0")
    
    override func didMove(to view: SKView) {
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -7.0)
        physicsWorld.contactDelegate = self
        sound = UserDefaults.standard.bool(forKey: "silenzioso")
        
        label = setLabel()
        addChild(label)
        parker.position = CGPoint(x: 100, y: 160)
        parker.size = CGSize(width: 60, height: 120)
        parker.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 40, height: 90))
        parker.physicsBody?.linearDamping = 0
        createBackground()
        parker.physicsBody?.mass = 0
        PrepareToPlay()
        reportAllAchievementsForGameState(hasWon: true)
        valore = 0
        
        
        addChild(parker)
        let tx0 =  SKTexture(image: UIImage(named: "sprite_0")!)
        let tx1 =  SKTexture(image: UIImage(named: "sprite_1")!)
        let tx2 =  SKTexture(image: UIImage(named: "sprite_2")!)
        let tx3 =  SKTexture(image: UIImage(named: "sprite_3")!)
        
        let n = SKAction.animate(with: [tx0,tx1,tx2,tx3], timePerFrame: 0.05)
        parker.run(SKAction.repeatForever(n))
        
        //aggiungo i Carabinieri in modo infinito ad intervalli di 1 secondo dall'altro
       
            self.run(SKAction.repeatForever(
             
                SKAction.sequence([
                    SKAction.run(self.addNemico),
                    SKAction.wait(forDuration: self.Spawn)
                ])
                
            ))
        
        
        
        parker.physicsBody?.categoryBitMask = categoryp
        parker.physicsBody?.collisionBitMask = categoryn | categoryn2 | categoryg
        parker.physicsBody?.contactTestBitMask = categoryn | categoryn2 | categoryg
        parker.name = "parker"
        parker.physicsBody?.restitution = 0
        
    }
    
    func setLabel() -> SKLabelNode{
        let myLabel = SKLabelNode(fontNamed: "ThaleahFat")
        myLabel.fontSize = 30
        myLabel.position = CGPoint(x: ((self.scene?.size.width)!) - 100 , y: (self.scene?.size.height)! - 100)
        return myLabel
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if muovichild{
            
            if Int(parker.position.y) <= 142
            {
                tapCount = 0
                doppioSalto = false
            }
            
            for t in touches {
                
                print(tapCount)
                if tapCount == 0
                {
                    jump()
                    tapCount += 1
                }
                else if tapCount == 1
                {
                    if(doppioSalto){
                    doubleJump()
                    doppioSalto = false
                    
                    }
                }
                
                
            }
        }
        else
        {
            for touch in touches {
                let location = touch.location(in: self)
                let node : SKNode = self.atPoint(location)
                if ((UserDefaults.standard.bool(forKey: "silenzioso")) && (node.name == "restart" ) || ( node.name == "leaderboard") || (node.name == "homeButton"))
                {
                    //
                    suono()
                }
                if node.name == "restart" {
                    
                    self.removeAllChildren()
                    guard let skView = self.view as SKView? else {
                        print("Could not get Skview")
                        return
                    }
                    
                    var scene = GameScene(size: skView.bounds.size)
                    
                    skView.presentScene(scene)
                }
                
                if node.name == "leaderboard" {
                    let viewController = GKGameCenterViewController()
                    viewController.viewState = .leaderboards
                    viewController.leaderboardIdentifier="This"
                    viewController.gameCenterDelegate = self
                    UIApplication.topViewController()?.present(
                        viewController,
                        animated: true, completion: nil)
                    
                }
                
                if node.name == "homeButton"{
                    
                    self.removeAllChildren()
                    guard let skView = self.view as SKView? else {
                        print("Could not get Skview")
                        return
                    }
                    
                    var scene = Main_Menu(fileNamed: "MainMenu")
                    
                    
                    skView.presentScene(scene)
                    
                }
            }
            
        }
        
    }
    func createBackground(){
        let sun = SKSpriteNode(imageNamed: "bg_noSun")
        sun.name = "bg"
        sun.size = CGSize(width: size.width, height: size.height)
        
        
        
        sun.position = CGPoint(x: size.width/2, y: size.height/2)
        sun.zPosition = -6
        
        addChild(sun)
        for i in 0...20{
            let back = SKSpriteNode(imageNamed: "city")
            back.name = "city"
            back.size = CGSize(width: size.width, height: 250)
            back.anchorPoint = CGPoint(x: 0.7, y: 0.7)
            back.position = CGPoint(x: CGFloat(i)*back.size.width, y: 290)
            back.zPosition = -5
            addChild(back)
        }
        
        
        for i in 0...20{
            let ground = SKSpriteNode(imageNamed: "strada")
            ground.size = CGSize(width:2100, height: 130)
            ground.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:ground.size.width+1000000, height: -70))
            ground.name = "ground"
            ground.physicsBody?.collisionBitMask = categoryp
            ground.physicsBody?.categoryBitMask = categoryg
            //        ground.physicsBody?.contactTestBitMask = categoryp
            ground.physicsBody?.isDynamic = false
            ground.anchorPoint = CGPoint(x: 1, y: 1)
            ground.physicsBody?.affectedByGravity = false
            ground.physicsBody?.restitution = 0
            ground.position = CGPoint(x: CGFloat(i)*ground.size.width, y: 120)
            ground.zPosition = -4
            addChild(ground)
        }
        
    }
    func movebackground(){
        
        self.enumerateChildNodes(withName: "city") { (node, error) in
            node.position.x -= self.tempo
            if node.position.x < -((self.scene?.size.width)!){
                node.position.x += (self.scene?.size.width)! * 20
                
            }
        }
        self.enumerateChildNodes(withName: "ground") { (node, error) in
            node.position.x -= self.tempo
            if node.position.x < -((self.scene?.size.width)!){
                node.position.x += (self.scene?.size.width)! * 20
                
            }
        }
    }
    
    func touchDown(atPoint pos: CGPoint) {
        
        
    }
    
    
    func jump() {
        
        //parker.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 600))
        parker.physicsBody?.apply(character: parker, completion: { (Bool) in
            self.doppioSalto = true
            
        })
        
    
        
        
    }
    func creaPopuo(){
        let gameover = SKSpriteNode(imageNamed: "Game Over")
        gameover.zPosition = 500
        gameover.position = CGPoint(x: (scene?.frame.midX)!, y: ((scene?.frame.maxY)!) + 300)
        addChild(gameover)
        
        let scoreimg = SKSpriteNode(imageNamed: "score")
        scoreimg.scale(to: CGSize(width: 230, height: 40))
        scoreimg.zPosition = 501
        scoreimg.position = CGPoint(x: ((scene?.frame.midX)!), y: ((scene?.frame.midY)!) + 300)
        addChild(scoreimg)
        
        
        var restartButton  = SKSpriteNode(imageNamed: "restart")
        restartButton.name = "restart"
        restartButton.scale(to: CGSize(width: 100, height: 50))
        restartButton.zPosition = 510
        restartButton.position = CGPoint(x: ((scene?.frame.midX)!), y: ((scene?.frame.midY)!) + 300)
        addChild(restartButton)
        
        var leaderboardButton = SKSpriteNode(imageNamed: "Coppa")
        leaderboardButton.name = "leaderboard"
        leaderboardButton.scale(to: CGSize(width: 50, height: 50))
        leaderboardButton.zPosition = 510
        leaderboardButton.position = CGPoint(x: ((scene?.frame.midX)!), y: ((scene?.frame.midY)!) + 300)
        addChild(leaderboardButton)
        
        var homeButton = SKSpriteNode(imageNamed: "Home")
        homeButton.name = "homeButton"
        homeButton.scale(to: CGSize(width: 55, height: 55))
        homeButton.zPosition = 510
        homeButton.position = CGPoint(x: ((scene?.frame.midX)!), y: ((scene?.frame.midY)!) + 300)
        addChild(homeButton)
        
        let alert = SKSpriteNode(imageNamed: "Alert")
        alert.name = "alert"
        alert.scale(to: CGSize(width: 250, height: 250))
        alert.position = CGPoint(x: ((scene?.frame.midX)!), y: ((scene?.frame.midY)!) + 300)
        //        let c = SKShapeNode(rect: CGRect(x: -100, y: 100
        //            , width: 270, height: 270))
        //        c.fillColor = .red
        let a = SKShapeNode(rect: CGRect(x: (scene?.position.x)!-667, y: ((scene?.position.y)!-400), width: 8000, height: (8000)))
        let purple = UIColor.black
        let purpleTrans = UIColor.withAlphaComponent(purple)(0.8)
        a.fillColor = purpleTrans
        addChild(a)
        a.zPosition = 2
        //        c.fillColor = .red
        addChild(alert)
        alert.zPosition = 10
        DispatchQueue.main.async {
            
            
            alert.run((SKAction.moveTo(y: ((self.scene?.frame.midY)!), duration: 0.3))) {
                // scoreimg.position=CGPoint(x: alert.frame.midX , y: alert.frame.midY + 60)
                scoreimg.run(SKAction.move(to: CGPoint(x: alert.frame.midX, y: alert.frame.midY + 60), duration: 0.2)) {
                    self.label.run(SKAction.move(to: CGPoint(x: scoreimg.frame.maxX - 35 , y: scoreimg.frame.midY - 8), duration: 0.3))
                }
                
                restartButton.run(SKAction.move(to: CGPoint(x: alert.frame.midX, y: alert.frame.midY - 60), duration: 0.2))
                
                leaderboardButton.run(SKAction.move(to: CGPoint(x: alert.frame.midX + 85  , y: alert.frame.midY - 60), duration: 0.2))
                
                homeButton.run(SKAction.move(to: CGPoint(x: alert.frame.midX - 85  , y: alert.frame.midY - 60), duration: 0.2))
                gameover.run(SKAction.move(to: CGPoint(x: alert.frame.midX   , y: alert.frame.maxY + 10), duration: 0.2))
                
                self.label.zPosition = 520
                
                
            }
        }
        
    }
    
    func doubleJump() {
        
        parker.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 200))
        
        
    }
    
    
    
    
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    func touchUp(atPoint pos: CGPoint) {
        parker.texture = SKTexture(imageNamed: "sprite_0")
    }
    
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    
    //ritorna un numero random compreso tra due numeri
    func random( min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    func getSoldi(){
        if soldi.isMultiple(of: 20){
            self.Spawn -= 0.05
        }
        DispatchQueue.global().async {
            
            if (!self.sound)
            {
                self.SuonoCoins.play()
            }
        }
        
        soldi+=1
        print(soldi)
        label.text = "$ "+String(soldi)
        if soldi == 1 {
            firstParker = 1
            reportAllAchievementsForGameState(hasWon: true)
        }
        if soldi == 10 {
            reportAllAchievementsForGameState(hasWon: true)
        }
        
        if soldi == 25 {
            reportAllAchievementsForGameState(hasWon: true)
        }
        if soldi == 50 {
                   reportAllAchievementsForGameState(hasWon: true)
               }
        if soldi == 75 {
                   reportAllAchievementsForGameState(hasWon: true)
               }
        if soldi == 100 {
                   reportAllAchievementsForGameState(hasWon: true)
               }
        
    }
    
    func addNemico() {
        DispatchQueue.main.async {
            
        
        //Creo il nodo che rappresenta il nemico
        let nemico = SKSpriteNode(imageNamed: "nem-1")
        
        let nemico2 = SKSpriteNode(imageNamed: "nem-2")
        
        
        // posiziono il nemico assegnandogli una x e la y random generata prima
        nemico.position = CGPoint(x: 1000, y: 128)
        nemico.scale(to: CGSize(width: 40, height: 105))
        nemico2.position = CGPoint(x: 1300, y: 133)
        nemico2.scale(to: CGSize(width: 80, height: 100))
        
        nemico.zPosition = 0
            if(self.muovichild){
                self.addChild(nemico)
        }
        
        
        
        
        // creo una durata random della Sprite, questo influisce sulla velocità di movimento
            var durataRandom = self.max
        if(self.soldi != 1){
            
        
        if (self.soldi.isMultiple(of: 10))
        {
            if self.max > 1{
                self.max -= 0.5
                self.tempo+=1
            }else{
                self.max -= 0.01
                self.tempo += 0.05
            }
        }
        }
        
        
        
        // assegno un'azione al nemico, ovvero muoversi verso la coordinate x opposta
        let azioneMuovi = SKAction.move(to: CGPoint(x: -nemico.size.width/2, y: 128), duration: TimeInterval(durataRandom))
        
        let azioneMuovi2 = SKAction.move(to: CGPoint(x: -nemico2.size.width/2 - 50, y: 133), duration: TimeInterval(durataRandom/1.5))
        
        
        
        //quando finisce l'azione rimuovo il nodo dalla View
        let azioneMuoviFinita = SKAction.removeFromParent()
        nemico.run(SKAction.sequence([azioneMuovi, azioneMuoviFinita])) {
            self.getSoldi()
        }
        nemico2.zPosition = 0
        nemico2.run(SKAction.sequence([azioneMuovi2, azioneMuoviFinita]))
        {
            self.getSoldi()
        }
            nemico.physicsBody?.categoryBitMask = self.categoryn
        nemico.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 40, height: 80))
        nemico.physicsBody?.affectedByGravity = false
        nemico.physicsBody?.collisionBitMask = 0
        nemico.name = "nemico"
            nemico.physicsBody?.contactTestBitMask = self.categoryp
        nemico2.physicsBody = SKPhysicsBody(rectangleOf: nemico2.frame.size)
        nemico2.physicsBody?.affectedByGravity = false
        nemico2.physicsBody?.collisionBitMask = 0
            nemico2.physicsBody?.contactTestBitMask = self.categoryp
            nemico2.physicsBody?.categoryBitMask = self.categoryn2
        nemico2.name = "nemico2"
        
        if(self.soldi == 19)
        {
            self.predi = self.predict()
            if(self.predi)
            {
                
                
                let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
                
                for _ in 1...3
                {
                    notificationFeedbackGenerator.prepare()
                    notificationFeedbackGenerator.notificationOccurred(.success)
                    notificationFeedbackGenerator.notificationOccurred(.warning)
                    notificationFeedbackGenerator.notificationOccurred(.error)
                }
                
                
                
                
            }
            
        }
        
        if(self.soldi >= 20 && self.soldi <= 30 || self.soldi >= 50)
        {
            
            if(self.rando > 3)
            {
                if(self.muovichild){
                    self.addChild(nemico2)
                }
            }
            self.predi = self.predict()
            if(self.predi)
            {
                
                let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
                notificationFeedbackGenerator.prepare()
                for _ in 1...3
                {
                    notificationFeedbackGenerator.prepare()
                    notificationFeedbackGenerator.notificationOccurred(.success)
                    notificationFeedbackGenerator.notificationOccurred(.success)
                    notificationFeedbackGenerator.notificationOccurred(.success)
                }
                
                
            }
            
        }
        }
        
    } 
    
    func predict() -> Bool{
        rando = random(min: 0, max: 6)
        if(rando > 3)
        {
            return true
        }else
        {
            return false
        }
    }
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        if ((contact.bodyA.node!.name == "nemico") && (contact.bodyB.node!.name == "nemico" ))||((contact.bodyA.node!.name == "nemico2") && (contact.bodyB.node!.name == "nemico" ))||((contact.bodyA.node!.name == "nemico") && (contact.bodyB.node!.name == "nemico2" ))||((contact.bodyA.node!.name == "nemico2") && (contact.bodyB.node!.name == "nemico2" ))||((contact.bodyA.node!.name == "parker") && (contact.bodyB.node!.name == "ground" ))||((contact.bodyA.node!.name == "ground") && (contact.bodyB.node!.name == "parker" ) )
        {
            
        }
        else
        {
            
            reportAllAchievementsForGameState(hasWon: true)
            if contact.bodyA.node?.name == "nemico" && contact.bodyB.node?.name == "parker" || contact.bodyA.node?.name == "parker" && contact.bodyB.node?.name == "nemico"{
                self.impact = 1
                self.impactMoto = 0
            }else{
                self.impactMoto = 1
                self.impact = 0
            }
            self.enumerateChildNodes(withName: "nemico") { (child, non) in
                child.removeAllActions()
                
                print("impatto")
                
                
            }
            
            self.enumerateChildNodes(withName: "nemico2") { (child, non) in
                child.removeAllActions()
                
                print("impatto Moto")
               
                
                
            }
            
            //removeAllChildren()
            removeAllActions()
            if label.text != nil{
                var scoree = label.text?.replacingOccurrences(of: "$", with: "")
                
                
                scoree = scoree?.replacingOccurrences(of: " ", with: "")
                saveHighscore(score: Int(scoree!)!)
                
            }
            
            if var c = UserDefaults.standard.array(forKey: "score")
            {
                AppendScore(score: c as! [Int])
            }else{
                IniziaScore()
            }
            if muovichild{
                //                let gameover = SKSpriteNode(imageNamed: "Game Over")
                //                gameover.zPosition = 1000
                //                gameover.position = CGPoint(x: (scene?.frame.midX)!, y: ((scene?.frame.maxY)!)-50)
                //                addChild(gameover)
                creaPopuo()
                parker.removeAllActions()
                muovichild = false
            }
        
        }
    }
    func reportScoreToGameCenter(score: Int64) {
        GameKitHelper.sharedInstance.reportScore(score: score,
                                                 forLeaderboardID:
            "grp.unlicensedparker.leaderboard")
    }
    
    override func update(_ currentTime: TimeInterval) {
        if(muovichild){
            DispatchQueue.main.async {
                self.movebackground()
            }
            
        }
    }
    func IniziaScore(){
        var arr = [Int]()
        arr.append(self.soldi)
        UserDefaults.standard.set(arr, forKey: "score")
        UserDefaults.standard.synchronize()
    }
    func AppendScore(score : [Int]){
        var sc = score
        sc.append(self.soldi)
        UserDefaults.standard.set(sc, forKey: "score")
        UserDefaults.standard.synchronize()
        self.portaScore = sc
    }
    func saveHighscore(score:Int) {
        
        //check if user is signed in
        if GKLocalPlayer.local.isAuthenticated {
            
            let scoreReporter = GKScore(leaderboardIdentifier: "this")
            
            scoreReporter.value = Int64(score)
            
            let scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.report(scoreArray, withCompletionHandler: {error -> Void in
                if error != nil {
                    print("error")
                }
                print(error)
                
            })
        }
    }
    //MARK: - GAME CENTER METHOD
    func reportAllAchievementsForGameState(hasWon: Bool) {
        var achievements: [GKAchievement] = []
        achievements.append(AchievementsHelper.sharedInstance.firstPlayAchievement(noOfSoldi: valore))
        achievements.append(AchievementsHelper.sharedInstance.niceStartAchievement(noOfParker: firstParker))
        achievements.append(AchievementsHelper.sharedInstance.impactParkerAchievement(noOfImpact: impact))
        achievements.append(AchievementsHelper.sharedInstance.impactMotoAchievement(noOfImpactMoto: impactMoto))
        achievements.append(AchievementsHelper.sharedInstance.risparmio25Achievement(noOfMonete: soldi))
        achievements.append(AchievementsHelper.sharedInstance.risparmio50Achievement(noOfMonete: soldi))
        achievements.append(AchievementsHelper.sharedInstance.risparmio75Achievement(noOfMonete: soldi))
        achievements.append(AchievementsHelper.sharedInstance.risparmio100Achievement(noOfMonete: soldi))
        print(achievements)
        GameKitHelper.sharedInstance.reportAchievements(achievements: achievements)
        
        
        
    }
    
    func retrieveBestScore() {
        if GKLocalPlayer.local.isAuthenticated {
            
            // Initialize the leaderboard for the current local player
            var gkLeaderboard = GKLeaderboard(players: [GKLocalPlayer.local])
            gkLeaderboard.identifier = "this"
            gkLeaderboard.timeScope = GKLeaderboard.TimeScope.allTime
            
            // Load the scores
            gkLeaderboard.loadScores(completionHandler: { (scores, error) -> Void in
                print(scores)
                if error == nil {
                    
                    
                }
                print(error)
                
            })
        }
    }
    func suono () {
        DispatchQueue.global().async {
            if(!self.sound){
                
                self.SuonoTap.play()
            }
        }
    }
    func PrepareToPlay(){
        DispatchQueue.global().async {
            let path = Bundle.main.path(forResource: "coins.wav", ofType:nil)!
            let url = URL(fileURLWithPath: path)
            do {
                
                self.SuonoCoins = try AVAudioPlayer(contentsOf: url)
                
                self.SuonoCoins.prepareToPlay()
                
                
            }
            catch {
                print("nada")
            }
            do {
                let path = Bundle.main.path(forResource: "tap.wav", ofType:nil)!
                let url = URL(fileURLWithPath: path)
                self.SuonoTap = try AVAudioPlayer(contentsOf: url)
                self.SuonoTap.prepareToPlay()
                
            }
            catch {
                print("nada")
                
            }
        }
    }
    
}
extension SKPhysicsBody{
    func apply(character : SKSpriteNode , completion : @escaping(Bool) -> Void){
        character.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 600))
        _ = Timer.scheduledTimer(withTimeInterval: 0.47, repeats: false, block: { (ciao) in
            ciao.invalidate()
            completion(true)
        })
    }
}

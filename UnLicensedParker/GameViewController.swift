//
//  GameViewController.swift
//  UnLicensedParker
//
//  Created by Luigi Mazzarella on 24/01/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = Main_Menu(fileNamed: "MainMenu")
        let skView = view as! SKView
        
        //FPS
        skView.showsFPS = false
        skView.showsPhysics = false
        //NODI
        skView.showsNodeCount = false
        //Ordine renderizzazione nodi
        skView.ignoresSiblingOrder = true
        //Rappresentazione scena
        scene!.scaleMode = .resizeFill
        //Aggiungo la scena alla view
        skView.presentScene(scene)
    }
    
    //Hide StatusBar
    
}

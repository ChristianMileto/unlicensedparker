//
//  RecognizeMachine.swift
//  UnLicensedParker
//
//  Created by Silvio Fosso on 16/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import UIKit
class Recognize{
    static let SharedInstance = Recognize()
    
    
    
    
    func returnValue(Device: String) -> String{
        return Device.westernArabicNumeralsOnly
    }
    
    
    func returnString(Device : String) -> String{
        var value_of_return = ""
        let array = Array(Device)
        for i in 0...array.count - 1{
            if(array[i] >= "a" && array[i] <= "z" || array[i] >= "A" && array[i] <= "Z")
            {
                value_of_return += String(array[i])
            }else{
                return value_of_return
            }
        }
        return ""
        
        
    }
    
    func main(Device : String) -> CGFloat{
        let device = returnString(Device: Device)
        let num_of_device = Int(returnValue(Device: Device))!
        
        if device.lowercased() == "iphone"{
            switch num_of_device {
            case 71..<85:
                /*@"iPhone7,1" on iPhone 6 Plus
                @"iPhone7,2" on iPhone 6
                @"iPhone8,1" on iPhone 6S
                @"iPhone8,2" on iPhone 6S Plus
                @"iPhone8,4" on iPhone SE*/
                return 290
                
            case 91..<95:
                /*@"iPhone9,1" on iPhone 7 (CDMA)
                @"iPhone9,3" on iPhone 7 (GSM)
                @"iPhone9,2" on iPhone 7 Plus (CDMA)
                @"iPhone9,4" on iPhone 7 Plus (GSM)*/
                return 295
                
                
            case 101,104,102,105:
                /*@"iPhone10,1" on iPhone 8 (CDMA)
                @"iPhone10,4" on iPhone 8 (GSM)
                @"iPhone10,2" on iPhone 8 Plus (CDMA)
                @"iPhone10,5" on iPhone 8 Plus (GSM)*/
                return 310
               
                
            case 103,106,112,114,116,118:
                
                /*@"iPhone10,3" on iPhone X (CDMA)
                @"iPhone10,6" on iPhone X (GSM)
                @"iPhone11,2" on iPhone XS
                @"iPhone11,4" on iPhone XS Max
                @"iPhone11,6" on iPhone XS Max China
                 @"iPhone11,8" on iPhone XR*/
                return 365
                
            case 121..<126:
              
                
               /* @"iPhone12,1" on iPhone 11
                @"iPhone12,3" on iPhone 11 Pro
                @"iPhone12,5" on iPhone 11 Pro Max*/
                return 370
            default:
                break
            }
            
            
        }else if device.lowercased() == "ipad"{
            return 330
            
        }
        return 300
    }
}
extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}
extension String {
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .flatMap { pattern ~= $0 ? Character($0) : nil })
    }
}
